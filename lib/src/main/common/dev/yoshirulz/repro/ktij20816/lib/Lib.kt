@file:Suppress("NOTHING_TO_INLINE")
/*
 * Side note: can you please add a Gradle option to suppress warnings project-wide?
 * I have a library full of inline functions and have to put this suppression in just about every file.
 * And a RedundantVisibilityModifier suppression in every test file, because the explicitApi Gradle setting doesn't seem to apply to test code but I add modifiers anyway.
 */

package dev.yoshirulz.repro.ktij20816.lib

public inline fun approximatePi(): Int
	= 3
