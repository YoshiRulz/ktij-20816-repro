@file:Suppress("SpellCheckingInspection")

import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

plugins {
	`maven-publish`
	kotlin("multiplatform") version "1.7.10"
}

group = "dev.yoshirulz.repro.ktij20816"
version = "0.1.0"

repositories {
	mavenCentral()
}

kotlin {
	jvm {
		val javaTargetVersion = JavaLanguageVersion.of(17)
		jvmToolchain {
			languageVersion.set(javaTargetVersion)
		}
		compilations.all {
			kotlinOptions.jvmTarget = javaTargetVersion.toString()
		}
	}

	explicitApi()

	@Suppress("UNUSED_VARIABLE")
	sourceSets {
		fun creatingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {})
			= creating {
				parent?.let(::dependsOn)
				configuration(this)
			}

		fun gettingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {})
			= getting {
				parent?.let(::dependsOn)
				configuration(this)
			}

		fun depStr(partialCoords: String, version: String)
			= "$partialCoords:$version"

		fun kotlinx(module: String, version: String)
			= depStr("org.jetbrains.kotlinx:kotlinx-$module", version)

		fun coroutines(module: String)
			= kotlinx("coroutines-$module", "1.6.4")

		fun ktor(module: String)
			= depStr("io.ktor:ktor-$module", "2.0.3")

		val commonMain by gettingWithParent(null)

		val jvmMain by gettingWithParent(commonMain)

		all {
			kotlin.srcDir("src/${name.takeLast(4).toLowerCase()}/${name.dropLast(4)}")
		}
	}
}
