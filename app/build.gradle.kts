@file:Suppress("SpellCheckingInspection")

import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

plugins {
	application
	kotlin("multiplatform") version "1.7.10"
	id("com.github.johnrengelman.shadow") version "7.1.2"
}

repositories {
	mavenCentral()
	mavenLocal()
}

kotlin {
	jvm {
		val javaTargetVersion = JavaLanguageVersion.of(17)
		jvmToolchain {
			languageVersion.set(javaTargetVersion)
		}
		withJava()
		compilations.all {
			kotlinOptions.jvmTarget = javaTargetVersion.toString()
		}
		project.application {
			mainClass.set("dev.yoshirulz.repro.ktij20816.app.MainKt")
		}
	}

	explicitApi()

	@Suppress("UNUSED_VARIABLE")
	sourceSets {
		fun creatingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {})
			= creating {
				parent?.let(::dependsOn)
				configuration(this)
			}

		fun gettingWithParent(parent: KotlinSourceSet?, configuration: KotlinSourceSet.() -> Unit = {})
			= getting {
				parent?.let(::dependsOn)
				configuration(this)
			}

		val commonMain by gettingWithParent(null) {
			dependencies {
				implementation("dev.yoshirulz.repro.ktij20816:lib:0.1.0")
			}
		}

		val jvmMain by gettingWithParent(commonMain)

		all {
			kotlin.srcDir("src/${name.takeLast(4).toLowerCase()}/${name.dropLast(4)}")
		}
	}
}
