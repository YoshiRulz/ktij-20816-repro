package dev.yoshirulz.repro.ktij20816.app

import dev.yoshirulz.repro.ktij20816.lib.approximatePi

internal fun doApp() {
	val approx = approximatePi() // error appears here
	println("pi is approximately $approx")
}
