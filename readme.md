for [KTIJ-20816](https://youtrack.jetbrains.com/issue/KTIJ-20816)

repro steps:
1. change to `/lib` and run `gradle :build :publishToMavenLocal`
2. open `/app/build.gradle.kts` in IDEA, navigate to `App.kt`, observe error
3. run `gradle :run` via IDEA to confirm error is incorrect
